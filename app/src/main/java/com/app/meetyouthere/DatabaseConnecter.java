package com.app.meetyouthere;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

public class DatabaseConnecter {
    private final String insertURL = "http://146.185.161.41/insert.php";
    private final String updateURL = "http://146.185.161.41/update.php";
    private final String selectURL = "http://146.185.161.41/select.php";
    private final String deleteURL = "http://146.185.161.41/delete.php";

    public String selectAllActivities() throws IOException {
        URL url = new URL(selectURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("type", "all_activities");
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");;

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);;
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String response = "";
        for (int c; (c = in.read()) >= 0;)
            response += String.valueOf((char)c);
        Log.d("Response from Server", response);
        return response;
    }

    public String selectAllComments() throws IOException {
        URL url = new URL(selectURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("type", "all_comments");
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");;

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);;
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String response = "";
        for (int c; (c = in.read()) >= 0;)
            response += String.valueOf((char)c);
        Log.d("Response from Server", response);
        return response;
    }

    public String selectAllUsers() throws IOException {
        URL url = new URL(selectURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("type", "all_users");
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");;

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);;
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String response = "";
        for (int c; (c = in.read()) >= 0;)
            response += String.valueOf((char)c);
        Log.d("Response from Server", response);
        return response;
    }

    public String selectUser(String userId) throws IOException {
        URL url = new URL(selectURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("type", "user_by_id");
        params.put("id", userId);
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");;

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);;
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String response = "";
        for (int c; (c = in.read()) >= 0;)
            response += String.valueOf((char)c);
        Log.d("Response from Server", response);
        return response;
    }

    public String updateActivity(String id, String creator_id, String participants_id, String date, String description, String category_id, String expiration, String max_participants, String location) throws IOException {
        Log.d("Response from Server", "test");
        URL url = new URL(updateURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("type", "activity");
        params.put("id", id);
        params.put("creator_id", creator_id);
        params.put("participants_id", participants_id);
        params.put("date", date);
        params.put("description", description);
        params.put("category_id", category_id);
        params.put("expiration", expiration);
        params.put("max_participants", max_participants);
        params.put("location", location);
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");;

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);;
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String response = "";
        for (int c; (c = in.read()) >= 0;)
            response += String.valueOf((char)c);
        Log.d("Response from Server", response);
        return response;
    }

    public String updateComment(String id, String commenter_id, String activity_id, String comment) throws IOException {
        URL url = new URL(updateURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("type", "comment");
        params.put("id", id);
        params.put("commenter_id", commenter_id);
        params.put("activity_id", activity_id);
        params.put("comment", comment);
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");;

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);;
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String response = "";
        for (int c; (c = in.read()) >= 0;)
            response += String.valueOf((char)c);
        Log.d("Response from Server", response);
        return response;
    }

    public String updateUser(String id, String email, String password, String name, String age, String gender, String nationality, String hobbies, String languages, String picture, String followers_id) throws IOException {
        URL url = new URL(updateURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("type", "user");
        params.put("id", id);
        params.put("email", email);
        params.put("password", password);
        params.put("name", name);
        params.put("age", age);
        params.put("gender", gender);
        params.put("nationality", nationality);
        params.put("hobbies", hobbies);
        params.put("languages", languages);
        params.put("picture", picture);
        params.put("followers_id", followers_id);
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");;

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);;
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String response = "";
        for (int c; (c = in.read()) >= 0;)
            response += String.valueOf((char)c);
        Log.d("Response from Server", response);
        return response;
    }

    public String insertActivity(String creator_id, String participants_id, String date, String description, String category_id, String expiration, String max_participants, String location) throws IOException {
        Log.d("Response from Server", "test");
        URL url = new URL(insertURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("type", "activity");
        params.put("creator_id", creator_id);
        params.put("participants_id", participants_id);
        params.put("date", date);
        params.put("description", description);
        params.put("category_id", category_id);
        params.put("expiration", expiration);
        params.put("max_participants", max_participants);
        params.put("location", location);
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");;

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);;
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String response = "";
        for (int c; (c = in.read()) >= 0;)
            response += String.valueOf((char)c);
        Log.d("Response from Server", response);
        return response;
    }

    public String insertComment(String commenter_id, String activity_id, String comment) throws IOException {
        URL url = new URL(insertURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("type", "comment");
        params.put("commenter_id", commenter_id);
        params.put("activity_id", activity_id);
        params.put("comment", comment);
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");;

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);;
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String response = "";
        for (int c; (c = in.read()) >= 0;)
            response += String.valueOf((char)c);
        Log.d("Response from Server", response);
        return response;
    }

    public String insertUser(String email, String password, String name, String age, String gender, String nationality, String hobbies, String languages, String picture, String followers_id) throws IOException {
        URL url = new URL(insertURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("type", "user");
        params.put("email", email);
        params.put("password", password);
        params.put("name", name);
        params.put("age", age);
        params.put("gender", gender);
        params.put("nationality", nationality);
        params.put("hobbies", hobbies);
        params.put("languages", languages);
        params.put("picture", picture);
        params.put("followers_id", followers_id);
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");;

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);;
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String response = "";
        for (int c; (c = in.read()) >= 0;)
            response += String.valueOf((char)c);
        Log.d("Response from Server", response);
        return response;
    }

    public String deleteActivity(String id) throws IOException {
        URL url = new URL(deleteURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("type", "activity_by_id");
        params.put("id", id);
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");;

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);;
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String response = "";
        for (int c; (c = in.read()) >= 0;)
            response += String.valueOf((char)c);
        Log.d("Response from Server", response);
        return response;
    }

    public String deleteComment(String id) throws IOException {
        URL url = new URL(deleteURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("type", "comment_by_id");
        params.put("id", id);
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");;

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);;
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String response = "";
        for (int c; (c = in.read()) >= 0;)
            response += String.valueOf((char)c);
        Log.d("Response from Server", response);
        return response;
    }

    public String deleteUser(String id) throws IOException {
        URL url = new URL(deleteURL);
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("type", "user_by_id");
        params.put("id", id);
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");;

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);;
        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String response = "";
        for (int c; (c = in.read()) >= 0;)
            response += String.valueOf((char)c);
        Log.d("Response from Server", response);
        return response;
    }
}