package com.app.meetyouthere;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

public class CategoryFragment extends Fragment {

    public CategoryFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);



        //CHANGE INTENTS WHEN WE HAVE CATEGOREIS RIGHT
        TextView sports = (TextView) view.findViewById(R.id.sports_next);
        sports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AppMainWindow.class);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Fonts initialization
        Typeface icons = FontManager.getTypeface(getActivity().getApplicationContext(), IconManager.ICONS);
        IconManager.markAsIconContainer(getActivity().findViewById(R.id.icons_container), icons);
    }


    @Override
    public void onResume() {
        super.onResume();

    }


}
