package com.app.meetyouthere;

import android.app.Activity;
import android.os.AsyncTask;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.meetyouthere.dummy.DummyContent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;


public class ActivityDetailFragment extends Fragment implements OnMapReadyCallback {

    private MapView mapView;
    private GoogleMap googleMap;
    Location location;

    public static final String ARG_ITEM_ID = "item_id";
    private JSONObject mItem;


    public ActivityDetailFragment() {
    }

    private class DatabaseConnectionActivities extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... s) {
            DatabaseConnecter databaseConnecter = new DatabaseConnecter();
            try {
                return databaseConnecter.selectAllActivities(); // MAKE SURE TO USE THE RIGHT AMOUNT OF PARAMETERS HERE (see tables below), AND MAKE SURE TO USE THE RIGHT METHOD (also see below)
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.

            try {
                String response = new DatabaseConnectionActivities().execute().get();
                JSONObject obj = null;
                try {
                    obj = new JSONObject(response);
                    JSONArray data = obj.getJSONArray("data");
                    mItem = data.getJSONObject(Integer.parseInt(getArguments().getString(ARG_ITEM_ID)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                try {
                    appBarLayout.setTitle("Name Event" + mItem.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            try {
                ((TextView) rootView.findViewById(R.id.activity_detail)).setText(mItem.getString("description"));
                ((TextView) rootView.findViewById(R.id.detail_location)).setText(mItem.getString("location"));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch(NullPointerException e){
                ((TextView) rootView.findViewById(R.id.detail_location)).setText("Location");
            }
        }
        mapView = (MapView) rootView.findViewById(R.id.map);

        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        GoogleMap googleMap = map;

        // Add a marker in Sydney and move the camera
        LatLng amsterdam = new LatLng(52.379189, 4.899431);
        map.addMarker(new MarkerOptions().position(amsterdam).title("Amsterdam"));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(amsterdam, 15));
        // map.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
    }
}