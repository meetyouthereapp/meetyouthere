package com.app.meetyouthere;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class NotificationFragment extends Fragment {

    // Array of strings storing country names
    String[] countries = new String[] {
            "Kevin",
            "Julita",
            "Thomas",
            "Karl",
            "Christine",
            "Niels",
            "Richard",
            "Char",
            "Tobi",
            "Asanto"
    };

    // Array of integers points to images stored in /res/drawable-ldpi/
    int[] flags = new int[]{
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user
    };

    // Array of strings to store time
    String[] currency = new String[]{
            "1 hr ago",
            "21 hrs ago",
            "20 mins ago",
            "15 hrs ago",
            "16 hrs ago",
            "2 mins ago",
            "15 hrs ago",
            "15 hrs ago",
            "15 hrs ago",
            "15 hrs ago"
    };

    public NotificationFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_notification, container, false);

        // Each row in the list stores country name, currency and flag
        List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();

        for(int i=0;i<10;i++){
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("txt", "Your activity was joined by " + countries[i]);
            hm.put("cur"," " + currency[i]);
            hm.put("flag", Integer.toString(flags[i]) );
            aList.add(hm);
        }

        // Keys used in Hashmap
        String[] from = { "flag","txt","cur" };

        // Ids of views in listview_layout
        int[] to = { R.id.user_image,R.id.username_notification,R.id.username_notification_time};

        // Instantiating an adapter to store each items
        // R.layout.listview_layout defines the layout of each item
        SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), aList, R.layout.notification_list_item, from, to);

        ListView listView = ( ListView ) rootView.findViewById(android.R.id.list);
        listView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Fonts initialization
        Typeface icons = FontManager.getTypeface(getActivity().getApplicationContext(), IconManager.ICONS);
        IconManager.markAsIconContainer(getActivity().findViewById(R.id.icons_container), icons);
    }
}
