package com.app.meetyouthere;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Terms extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        // Fonts initialization
        Typeface icons = FontManager.getTypeface(getApplicationContext(), IconManager.ICONS);
        IconManager.markAsIconContainer(findViewById(R.id.icons_container), icons);
    }

    public void cancelActivity(View v) {
        super.onBackPressed();
    }
}
