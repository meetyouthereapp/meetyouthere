package com.app.meetyouthere;

import android.content.Intent;
import android.graphics.Typeface;

import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class CreateActivity extends AppCompatActivity {


    public Button get_place;
    private PlacePicker.IntentBuilder builder;
    private static final int PLACE_PICKER_FLAG = 1;
    public TextView  myLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);


        // Assume thisActivity is the current activity
        int permissionCheck = ContextCompat.checkSelfPermission(CreateActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        builder = new PlacePicker.IntentBuilder();
        myLocation = (TextView) findViewById(R.id.myLocation);

        get_place = (Button) findViewById(R.id.pickerPlace);
        get_place.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    builder = new PlacePicker.IntentBuilder();
                    Intent intent = builder.build(CreateActivity.this);
                    // Start the Intent by requesting a result, identified by a request code.
                    startActivityForResult(intent, PLACE_PICKER_FLAG);

                } catch (GooglePlayServicesRepairableException e) {
                    GooglePlayServicesUtil
                            .getErrorDialog(e.getConnectionStatusCode(), CreateActivity.this, 0);
                } catch (GooglePlayServicesNotAvailableException e) {
                    Toast.makeText(CreateActivity.this, "Google Play Services is not available.",
                            Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
        // Fonts initialization
        Typeface icons = FontManager.getTypeface(getApplicationContext(), IconManager.ICONS);
        IconManager.markAsIconContainer(findViewById(R.id.icons_container), icons);



        TextView createActivityButton = (TextView) findViewById(R.id.create_activity);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PLACE_PICKER_FLAG:
                    Place place = PlacePicker.getPlace(data, this);
                    myLocation.setText(place.getAddress());
                    break;
            }
        }
    }


    public void cancelActivityCreate(View v) {

        super.onBackPressed();
    }

    public void createActivityCreate(View v) {

        final EditText e_activityTitle = (EditText)findViewById(R.id.editText_title);
        final EditText e_activityDescription = (EditText)findViewById(R.id.editText_description);
        final TextView e_activityLocation = (TextView)findViewById(R.id.myLocation);
        final EditText e_activityMaxPeople = (EditText)findViewById(R.id.editText_max);
        final EditText e_activityDate = (EditText)findViewById(R.id.editText_date);
        final EditText e_activityDueDate = (EditText)findViewById(R.id.editText_expire);

         Spinner editCategory = (Spinner) findViewById(R.id.editCategory);

//        System.out.println("DATEEEE!!!!:::    " + e_activityDate.getText().toString());
//        System.out.println("DESCRIPTION!!!!:::    " +  e_activityDescription.getText().toString());
//        System.out.println("Category!!!!:::    " + editCategory.getSelectedItem().toString());
//        System.out.println("DUE-DATEEEE!!!!:::    " +  e_activityDueDate.getText().toString());
//        System.out.println("MAX PEOPLE!!!!:::    " +  e_activityMaxPeople.getText().toString());
//        System.out.println("LOCATION!!!!:::    " +  e_activityLocation.getText().toString());



        try{
            String response = new DatabaseConnection().execute("", "" , e_activityDate.getText().toString(),
                    e_activityDescription.getText().toString(), editCategory.getSelectedItem().toString(),
                    e_activityDueDate.getText().toString(), e_activityMaxPeople.getText().toString(), e_activityLocation.getText().toString()).get();
        }catch (InterruptedException e){

        }catch(ExecutionException e){

        }

        Intent i = new Intent(CreateActivity.this, AppMainWindow.class);
        startActivity(i);

    }

    private class DatabaseConnection extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... s) {
            DatabaseConnecter databaseConnecter = new DatabaseConnecter();
            try {
                return databaseConnecter.insertActivity(s[0],s[1],s[2],s[3],s[4],s[5],s[6],s[7]); // MAKE SURE TO USE THE RIGHT AMOUNT OF PARAMETERS HERE (see tables below), AND MAKE SURE TO USE THE RIGHT METHOD (also see below)
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
