package com.app.meetyouthere;

import android.graphics.Typeface;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ActivityFeed extends ListFragment {

    // Array of strings storing country names
    String[] countries = new String[] {
            "Kevin",
            "Julita",
            "Thomas",
            "Karl",
            "Christine",
            "Niels",
            "Richard",
            "Char",
            "Tobi",
            "Asanto"
    };

    // Array of integers points to images stored in /res/drawable-ldpi/
    int[] flags = new int[]{
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user,
            R.mipmap.ic_user
    };

    // Array of strings to store time
    String[] currency = new String[]{
            "1 hr ago",
            "21 hrs ago",
            "20 mins ago",
            "15 hrs ago",
            "16 hrs ago",
            "2 mins ago",
            "15 hrs ago",
            "15 hrs ago",
            "15 hrs ago",
            "15 hrs ago"
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_activity_feed, container, false);

        // Each row in the list stores country name, currency and flag
        List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();

        for(int i=0;i<10;i++){
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("txt", "Activity by " + countries[i]);
            hm.put("cur"," " + currency[i]);
            hm.put("flag", Integer.toString(flags[i]) );
            aList.add(hm);
        }

        // Keys used in Hashmap
        String[] from = { "flag","txt","cur" };

        // Ids of views in listview_layout
        int[] to = { R.id.user_image,R.id.user_name,R.id.activity_notification_time};

        // Instantiating an adapter to store each items
        // R.layout.listview_layout defines the layout of each item
        SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), aList, R.layout.feed_item, from, to);

        ListView listView = ( ListView ) rootView.findViewById(android.R.id.list);
        listView.setAdapter(adapter);

        return  rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Fonts initialization
        Typeface icons = FontManager.getTypeface(getActivity().getApplicationContext(), IconManager.ICONS);
        IconManager.markAsIconContainer(getActivity().findViewById(R.id.icons_container), icons);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        System.out.println("Clicked list item");
    }
}
