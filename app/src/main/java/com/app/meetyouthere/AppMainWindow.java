package com.app.meetyouthere;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.app.meetyouthere.dummy.DummyContent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class AppMainWindow extends AppCompatActivity implements View.OnClickListener {

    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //MultiDex.install(getTargetContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_main_window);

        // Fonts initialization
        Typeface icons = FontManager.getTypeface(getApplicationContext(), IconManager.ICONS);
        IconManager.markAsIconContainer(findViewById(R.id.icons_container), icons);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.top_toolbar);
//        setSupportActionBar(toolbar);
//        toolbar.setTitle(getTitle());


        //ADD ACTIVITY TO LIST
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AppMainWindow.this, CreateActivity.class);
                startActivity(i);
            }
        });

        View recyclerView = findViewById(R.id.activity_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);

        if (findViewById(R.id.activity_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        FrameLayout feed = (FrameLayout)findViewById(R.id.toolbar_feed);
        FrameLayout category = (FrameLayout)findViewById(R.id.toolbar_category);
        FrameLayout chat = (FrameLayout)findViewById(R.id.toolbar_chat);
        FrameLayout notification = (FrameLayout)findViewById(R.id.toolbar_notification);
        FrameLayout user = (FrameLayout)findViewById(R.id.toolbar_user);

        feed.setOnClickListener(this);
        category.setOnClickListener(this);
        chat.setOnClickListener(this);
        notification.setOnClickListener(this);
        user.setOnClickListener(this);

    }

    private class DatabaseConnectionActivities extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... s) {
            DatabaseConnecter databaseConnecter = new DatabaseConnecter();
            try {
                return databaseConnecter.selectAllActivities(); // MAKE SURE TO USE THE RIGHT AMOUNT OF PARAMETERS HERE (see tables below), AND MAKE SURE TO USE THE RIGHT METHOD (also see below)
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private class DatabaseConnectionUsers extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... s) {
            DatabaseConnecter databaseConnecter = new DatabaseConnecter();
            try {
                return databaseConnecter.selectUser(s[0]); // MAKE SURE TO USE THE RIGHT AMOUNT OF PARAMETERS HERE (see tables below), AND MAKE SURE TO USE THE RIGHT METHOD (also see below)
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        try {
            String response = new DatabaseConnectionActivities().execute().get();
            JSONObject obj = null;
            try {
                obj = new JSONObject(response);
                JSONArray data = obj.getJSONArray("data");
//                JSONObject moredata = data.getJSONObject(4);
//                Log.d("DESCRIPTION IS: ", moredata.getString("description"));

                recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(data));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }



    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final JSONArray mValues;

        public SimpleItemRecyclerViewAdapter(JSONArray items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.activity_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            Log.d("Current position is: ", Integer.toString(position));
            try {
                holder.mItem = mValues.getJSONObject(position);

                String usernameJSON = new DatabaseConnectionUsers().execute(holder.mItem.getString("creator_id")).get();
                String username = new JSONObject(usernameJSON).getJSONArray("data").getJSONObject(0).getString("name");
                Log.d("USERNAME IS: ", username);
                holder.mIdView.setText(holder.mItem.getString("id"));
                holder.mUserNameView.setText(username);
                holder.mContentView.setText(holder.mItem.getString("description"));
                holder.mLocationView.setText(holder.mItem.getString("location"));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch(NullPointerException e){

            }


            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        try {
                            arguments.putString(ActivityDetailFragment.ARG_ITEM_ID, holder.mItem.getString("id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        ActivityDetailFragment fragment = new ActivityDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.activity_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, ActivityDetailActivity.class);
                        try {
                            intent.putExtra(ActivityDetailFragment.ARG_ITEM_ID, holder.mItem.getString("id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.length();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mIdView;
            public final TextView mUserNameView;
            public final TextView mLocationView;
            public final TextView mContentView;
            public JSONObject mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mUserNameView = (TextView) view.findViewById(R.id.user_name);
                mIdView = (TextView) view.findViewById(R.id.activity_time);
                mContentView = (TextView) view.findViewById(R.id.activity_title);
                mLocationView = (TextView) view.findViewById(R.id.detail_location);

            }

            @Override
            public String toString() {
                return super.toString() + " '" + mContentView.getText() + "'";
            }
        }
    }

    @Override
    public void onClick(View v) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (v.getId()){
            case R.id.toolbar_feed:

                Intent inent = new Intent(this, AppMainWindow.class);

                // calling an activity using <intent-filter> action name
                //  Intent inent = new Intent("com.hmkcode.android.ANOTHER_ACTIVITY");

                startActivity(inent);

                //ActivityFeed blankFragment = new ActivityFeed();
                //fragmentManager.beginTransaction().add(R.id.fragment_container, blankFragment).addToBackStack(null).commit();
                break;
            case R.id.toolbar_category:

                CategoryFragment categoryFragment = new CategoryFragment();
                fragmentManager.beginTransaction().add(R.id.fragment_container, categoryFragment).addToBackStack(null).commit();
                break;
            case R.id.toolbar_chat:

                ChatFragment chatFragment = new ChatFragment();
                fragmentManager.beginTransaction().add(R.id.fragment_container, chatFragment).addToBackStack(null).commit();
                break;
            case R.id.toolbar_user:

                UserProfileFragment userProfileFragment1 = new UserProfileFragment();
                fragmentManager.beginTransaction().add(R.id.fragment_container, userProfileFragment1).addToBackStack(null).commit();
                break;
            case R.id.toolbar_notification:

                NotificationFragment notificationFragment = new NotificationFragment();
                fragmentManager.beginTransaction().add(R.id.fragment_container, notificationFragment).addToBackStack(null).commit();
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }

    public void openUser(View v){
        Intent intent = new Intent(this, CreateActivity.class);
        startActivity(intent);
    }

}
