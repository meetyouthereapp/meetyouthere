package com.app.meetyouthere;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;

/**
 * Created by Karl on 17.6.2016.
 */
public class GalleryActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("intent","galleryactivity");
        galleryIntent();
    }

    public int REQUEST_GALLERY_PHOTO = 2;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Intent i = new Intent();
        Uri uri = data.getData();
        if (uri != null) {
            String path = uri.toString();
            i.putExtra("path", path);
            setResult(RESULT_OK, i);
        } else {
            Log.d("data was null","man");
        }
        finish();
    }

    private void galleryIntent() {
        Log.d("intent","galleryactivity launched");
        Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(galleryIntent, "Select File"), REQUEST_GALLERY_PHOTO);
    }
}