package com.app.meetyouthere;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class InviteFriends extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Fonts initialization
        Typeface icons = FontManager.getTypeface(getApplicationContext(), IconManager.ICONS);
        IconManager.markAsIconContainer(findViewById(R.id.icons_container), icons);
    }

    public void cancelActivity(View v) {
        super.onBackPressed();
    }


}
