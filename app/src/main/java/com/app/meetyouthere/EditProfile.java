package com.app.meetyouthere;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;

public class EditProfile extends AppCompatActivity {

    String userId = "4";
    EditText name, age, gender, nationality, languages, hobbies;
    Button updateProfile;
    String MY_URL_STRING;

    String _userName, _userAge, _userGender, _userNationality, _userHobby, _userLanguages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        name = (EditText)findViewById(R.id.name);
        age = (EditText)findViewById(R.id.age);
        nationality = (EditText)findViewById(R.id.nationality);
        gender = (EditText)findViewById(R.id.gender);
        hobbies = (EditText)findViewById(R.id.hobbies);
        languages = (EditText)findViewById(R.id.languages);


        updateProfile = (Button)findViewById(R.id.button);
        updateProfile.setOnClickListener(
                new View.OnClickListener()
                {
                    public void onClick(View view)
                    {
                        _userName = name.getText().toString();
                        _userAge = age.getText().toString();
                        _userNationality = nationality.getText().toString();
                        _userGender = gender.getText().toString();
                        _userHobby = hobbies.getText().toString();
                        _userLanguages = languages.getText().toString();

                        new updateUserInfo().execute(_userName, _userAge, _userNationality, _userGender, _userHobby, _userLanguages);
                        Toast.makeText(EditProfile.this, "Profile updated", Toast.LENGTH_SHORT).show();
                    }
                });

        getProfile(userId);

        new DownloadImageTask((ImageView) findViewById(R.id.profilePicture))
                .execute(MY_URL_STRING);
    }

    public class getUserInfo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... s) {
            DatabaseConnecter databaseConnecter = new DatabaseConnecter();
            try {
                return databaseConnecter.selectUser(s[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private class DatabaseConnectionUsers extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... s) {
            DatabaseConnecter databaseConnecter = new DatabaseConnecter();
            try {
                return databaseConnecter.selectUser(s[0]); // MAKE SURE TO USE THE RIGHT AMOUNT OF PARAMETERS HERE (see tables below), AND MAKE SURE TO USE THE RIGHT METHOD (also see below)
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class updateUserInfo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... s) {
            DatabaseConnecter databaseConnecter = new DatabaseConnecter();
            try {
                return databaseConnecter.updateUser(s[0],"demouser@demo.com","demopass", _userName, _userAge, _userGender, _userNationality, _userHobby, _userLanguages,s[1],"3,4");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(EditProfile.this, "Profile updated", Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    public String[] parseUserInfo(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            JSONArray data = obj.getJSONArray("data");
            JSONObject alldata = data.getJSONObject(0);

            String email = alldata.getString("email");
            String password = alldata.getString("password");
            String name = alldata.getString("name");
            String age = alldata.getString("age");
            String gender = alldata.getString("gender");
            String nationality = alldata.getString("nationality");
            String hobbies = alldata.getString("hobbies");
            String languages = alldata.getString("languages");
            String picture = alldata.getString("picture");
            String followers_id = alldata.getString("followers_id");

            String[] userInfo = {
                    email, password, name, age, gender, nationality, hobbies, languages, picture, followers_id
            };

            return userInfo;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void getProfile(String userId) {
        try {
            String response = new getUserInfo().execute(userId).get();
            try {
                JSONObject obj = new JSONObject(response);
                JSONArray data = obj.getJSONArray("data");
                JSONObject moredata = data.getJSONObject(0);
                String picture = moredata.getString("picture");
                MY_URL_STRING = picture;

                String userName = moredata.getString("name");
                String userAge = moredata.getString("age");
                String userGender = moredata.getString("gender");
                String userNationality = moredata.getString("nationality");
                String userHobby = moredata.getString("hobbies");
                String userLanguages = moredata.getString("languages");


                name = (EditText)findViewById(R.id.name);
                name.setText(userName);
                age = (EditText)findViewById(R.id.age);
                age.setText(userAge);
                nationality = (EditText)findViewById(R.id.nationality);
                nationality.setText(userNationality);
                gender = (EditText)findViewById(R.id.gender);
                gender.setText(userGender);
                hobbies = (EditText)findViewById(R.id.hobbies);
                hobbies.setText(userHobby);
                languages = (EditText)findViewById(R.id.languages);
                languages.setText(userLanguages);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
