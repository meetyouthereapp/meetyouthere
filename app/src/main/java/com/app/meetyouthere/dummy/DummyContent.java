package com.app.meetyouthere.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    public static class DummyItem {

        public String id;
        public String user;
        public String content;
        public String location;

        public DummyItem(String id, String user, String content, String location) {
            this.id = id;
            this.user = user;
            this.content = content;
            this.location = location;
        }

        @Override
        public String toString() {
            return content;
        }

        public String getUser(){
            return user;
        }

        public String getLocation() {
            return location;
        }
    }


    public static List<DummyItem> ITEMS = new ArrayList<DummyItem>();
    public static Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

    static {

        addItem(new DummyItem("01:50", "Niels", "Drinking beers", "Oorloff"));

        addItem(new DummyItem("01:50", "Niels", "Drinking beers", "Oorlof"));

        addItem(new DummyItem("02:00", "Charlotte", "Studying session", "Roeters Eiland"));
        addItem(new DummyItem("03:33", "Julita", "Soccer", "Westerpark"));
        addItem(new DummyItem("04:44", "Kevin", "Go to museum", "Museumplein"));
        addItem(new DummyItem("05:55", "Dorine", "BBQ", "Vondelpark"));
        addItem(new DummyItem("06:00", "Otteran", "Tanning", "Oosterpark"));

        addItem(new DummyItem("07:11", "Karl", "Music festival", "Twiske"));
        addItem(new DummyItem("08:12", "Mauritia", "Eating dinner", "Sumo Leidseplein"));
        addItem(new DummyItem("09:14", "Pam", "Watch Game Of Thrones together", "Adres"));
        addItem(new DummyItem("10:18", "Quirine", "Painting", "Adres"));
    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

}

