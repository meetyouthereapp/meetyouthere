package com.app.meetyouthere;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.ads.internal.request.StringParcel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;


public class UserProfileFragment extends Fragment implements View.OnClickListener{

    TextView setPictureButton;
    String userId = "4";
    String MY_URL_STRING;
    TextView name;

    public UserProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("You are in","UserProfileFragment.java");
        // Inflate the layout for this fragment
        View userProfileView = inflater.inflate(R.layout.fragment_user_profile, container, false);

        final ImageView iv = (ImageView) userProfileView.findViewById(R.id.profilePicture);
        name = (TextView) userProfileView.findViewById(R.id.user_name);

        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        // Set profile picture on start
        // TODO: Get userid from current user;
        String path = getProfilePic(userId);
        inflateProfilePic(path, iv);

        TextView terms = (TextView) userProfileView.findViewById(R.id.terms);
        TextView policy = (TextView) userProfileView.findViewById(R.id.private_policy);
        TextView about = (TextView) userProfileView.findViewById(R.id.about_app);
        TextView invite = (TextView) userProfileView.findViewById(R.id.invite_friends);
        TextView profileEdit = (TextView) userProfileView.findViewById(R.id.edit_profile);
        TextView profilePhoto = (TextView) userProfileView.findViewById(R.id.set_profile_picture);
        TextView logout = (TextView) userProfileView.findViewById(R.id.logout);

        terms.setOnClickListener(this);
        policy.setOnClickListener(this);
        about.setOnClickListener(this);
        invite.setOnClickListener(this);
        profileEdit.setOnClickListener(this);
        profilePhoto.setOnClickListener(this);
        logout.setOnClickListener(this);

        //new DownloadImageTask((ImageView) userProfileView.findViewById(R.id.profilePicture))
        // .execute(MY_URL_STRING);

        return userProfileView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Fonts initialization
        Typeface icons = FontManager.getTypeface(getActivity().getApplicationContext(), IconManager.ICONS);
        IconManager.markAsIconContainer(getActivity().findViewById(R.id.icons_container), icons);
    }

    public void showAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Choose action");
        alertDialogBuilder
                .setCancelable(true)
                .setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Open gallery
                        Log.d("Gallery launch","");
                        Intent galleryIntent = new Intent(getActivity(), GalleryActivity.class);
                        startActivityForResult(galleryIntent, 1);
                    }
                })
                .setNegativeButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Open camera
                        //TODO: Camera intent to capture new profile picture
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(takePictureIntent,2);
                    }
                })
                .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Cancel
                        Toast.makeText(getActivity(), "Canceled", Toast.LENGTH_SHORT).show();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            // Return from gallery
            Log.d("requestCode", "1");
            Log.d("your data is:", data.getStringExtra("path"));
            Uri galleryUri = Uri.parse(data.getStringExtra("path"));
            String realPath = RealPathUtil.getRealPathFromURI_API19(getActivity(), galleryUri);
            Log.d("realPath is", realPath);
            setProfilePic(userId,realPath);
            // Reload fragment
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.detach(this).attach(this).commit();
        } else if (requestCode == 2) {
            // Return from camera
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            String path = createImageFile(imageBitmap);
            setProfilePic(userId,path);
            // Reload fragment
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.detach(this).attach(this).commit();
        }
        else {
            Log.d("wtf", "fragment");
        }
    }

    public String createImageFile(Bitmap bm) {
        Log.d("createImageFile","start");
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/Pictures/");
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "MUT_" + timeStamp + ".jpg";
        File file = new File(myDir, imageFileName);
        String path = file.toString();
        try {
            Log.d("createImageFile","try");
            FileOutputStream out = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            return path;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void inflateProfilePic(String path, ImageView iv) {
        //ImageView iv = (ImageView) getActivity().findViewById(R.id.profilePicture);
        File file = new File(path);
        if(file.exists()) {
            Bitmap bmImg = BitmapFactory.decodeFile(path);
            int nh = (int) (bmImg.getHeight() * (512.0 / bmImg.getWidth()));
            Bitmap scaledImg = Bitmap.createScaledBitmap(bmImg, 512, nh, true);
            iv.setImageBitmap(scaledImg);
        } else {
            AssetManager assetManager = getContext().getAssets();
            try {
                InputStream istr = assetManager.open("default_profilepicture.png");
                Bitmap bmImg = BitmapFactory.decodeStream(istr);
                int nh = (int) (bmImg.getHeight() * (512.0 / bmImg.getWidth()));
                Bitmap scaledImg = Bitmap.createScaledBitmap(bmImg, 512, nh, true);
                iv.setImageBitmap(scaledImg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getProfilePic(String userId) {
        try {
            String response = new getUserInfo().execute(userId).get();
            try {
                JSONObject obj = new JSONObject(response);
                JSONArray data = obj.getJSONArray("data");
                JSONObject moredata = data.getJSONObject(0);
                String picture = moredata.getString("picture");
                MY_URL_STRING = picture;

                String userName = moredata.getString("name");
                name.setText(userName);

                if (picture != null) {
                    return picture;
                }
                return null;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setProfilePic(String userId, String path) {
        try {
            new updateUserInfo().execute(userId, path).get();
            Toast.makeText(getActivity(), "Profile picture updated", Toast.LENGTH_SHORT).show();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public class getUserInfo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... s) {
            DatabaseConnecter databaseConnecter = new DatabaseConnecter();
            try {
                return databaseConnecter.selectUser(s[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class updateUserInfo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... s) {
            DatabaseConnecter databaseConnecter = new DatabaseConnecter();
            try {
                return databaseConnecter.updateUser(s[0],"demouser@demo.com","demopass","Demo","33","m","Dutch","Football","Dutch",s[1],"3,4");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public String[] parseUserInfo(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            JSONArray data = obj.getJSONArray("data");
            JSONObject alldata = data.getJSONObject(0);

            String email = alldata.getString("email");
            String password = alldata.getString("password");
            String name = alldata.getString("name");
            String age = alldata.getString("age");
            String gender = alldata.getString("gender");
            String nationality = alldata.getString("nationality");
            String hobbies = alldata.getString("hobbies");
            String languages = alldata.getString("languages");
            String picture = alldata.getString("picture");
            String followers_id = alldata.getString("followers_id");

            String[] userInfo = {
                    email, password, name, age, gender, nationality, hobbies, languages, picture, followers_id
            };

            return userInfo;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.terms:

                Intent intent = new Intent(getActivity(), Terms.class);
                startActivity(intent);
                break;
            case R.id.invite_friends:

                Intent i = new Intent(getActivity(), InviteFriends.class);
                startActivity(i);
                break;
            case R.id.private_policy:

                Intent j = new Intent(getActivity(), PolicyActivity.class);
                startActivity(j);
                break;
            case R.id.about_app:

                Intent k = new Intent(getActivity(), About.class);
                startActivity(k);
                break;
            case R.id.set_profile_picture:

                showAlert();
                break;
            case R.id.edit_profile:

                Intent x = new Intent(getActivity(), EditProfile.class);
                startActivity(x);
                break;
            case R.id.logout:
                LoginManager.getInstance().logOut();
                Intent app = getActivity().getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getActivity().getBaseContext().getPackageName() );
                app.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(app);
                break;
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

}