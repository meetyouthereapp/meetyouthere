package com.app.meetyouthere;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class IconManager {

    public  static final String ROOT = "fonts/",
            ICONS = ROOT + "icon.ttf";

    public static Typeface getTypeface(Context context, String font){
        return Typeface.createFromAsset(context.getAssets(), font);
    }

    public  static  void markAsIconContainer(View v, Typeface typeface){
        if(v instanceof ViewGroup){
            ViewGroup vg = (ViewGroup) v;
            for(int i = 0; i < vg.getChildCount(); i++){
                View childAt = vg.getChildAt(i);
                markAsIconContainer(childAt, typeface);
            }
        }else if(v instanceof TextView) {
            ((TextView) v).setTypeface(typeface);
        }
    }

}
